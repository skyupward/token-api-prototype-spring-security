package top.ourway.prototype.springsecurity.rest.config;


import org.springframework.boot.autoconfigure.security.Http401AuthenticationEntryPoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import top.ourway.prototype.springsecurity.rest.security.*;

/**
 * Created by skyupward on 2015/5/23.
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.authenticationProvider(tokenAuthenticationProvider())
                .inMemoryAuthentication().withUser("user").password("password").roles("USER");

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
                .antMatchers("/secure/**").authenticated()
                .anyRequest().permitAll()
                .and()
                .addFilterBefore(tokenAuthenticationFilter(), LogoutFilter.class)
                .exceptionHandling().authenticationEntryPoint(new Http401AuthenticationEntryPoint(null))
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .csrf().disable()
                .formLogin()
                .failureHandler(new SimpleUrlAuthenticationFailureHandler())
                .successHandler(tokenAuthenticationSuccessHandler())
                .and()
                .logout()
                .addLogoutHandler(tokenLogoutHandler())
                .logoutSuccessHandler(tokenLogoutSuccessHandler());


    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public TokenAuthenticationSuccessHandler tokenAuthenticationSuccessHandler() {
        return new TokenAuthenticationSuccessHandler();
    }

    @Bean
    public TokenLogoutSuccessHandler tokenLogoutSuccessHandler() {
        return new TokenLogoutSuccessHandler();
    }

    @Bean
    public TokenLogoutHandler tokenLogoutHandler() {
        return new TokenLogoutHandler();
    }

    @Bean
    public TokenAuthenticationFilter tokenAuthenticationFilter() {
        return new TokenAuthenticationFilter();
    }

    @Bean
    public TokenAuthenticationProvider tokenAuthenticationProvider() {
        return new TokenAuthenticationProvider();
    }
}
