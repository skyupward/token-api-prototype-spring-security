package top.ourway.prototype.springsecurity.rest.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Created by skyupward on 2015/5/23.
 */
public class TokenAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private TokenManager tokenManager;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        TokenAuthenticationToken tokenAuthenticationToken = (TokenAuthenticationToken) authentication;
        UserDetails userDetails = tokenManager.getUserDetails((String) tokenAuthenticationToken.getCredentials());

        if (userDetails != null) {
            return new TokenAuthenticationToken(userDetails, authentication.getCredentials(),
                    userDetails.getAuthorities());
        }

        throw new BadCredentialsException("token is invalid!");

    }

    @Override
    public boolean supports(Class<?> authentication) {
        return TokenAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
