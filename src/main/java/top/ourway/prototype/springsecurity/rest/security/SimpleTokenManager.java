package top.ourway.prototype.springsecurity.rest.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by skyupward on 2015/5/23.
 */
@Component
public class SimpleTokenManager implements TokenManager {

    private ConcurrentHashMap<String, UserDetails> tokens = new ConcurrentHashMap<>();

    @Override
    public String createToken(UserDetails userDetails) {

        String token = UUID.randomUUID().toString();
        tokens.putIfAbsent(token, userDetails);
        return token;

    }

    @Override
    public UserDetails removeToken(String token) {
        return tokens.remove(token);
    }

    @Override
    public UserDetails getUserDetails(String token) {
        return tokens.get(token);
    }

}
