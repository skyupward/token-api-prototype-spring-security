package top.ourway.prototype.springsecurity.rest.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by skyupward on 2015/5/23.
 */
public class TokenLogoutHandler implements LogoutHandler {

    @Autowired
    private TokenManager tokenManager;

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {

        if (authentication != null && authentication instanceof TokenAuthenticationToken)
            tokenManager.removeToken((String) authentication.getCredentials());

    }
}
