package top.ourway.prototype.springsecurity.rest.config;

import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import top.ourway.prototype.springsecurity.rest.security.TokenAuthenticationFilter;

/**
 * Created by skyupward on 2015/5/23.
 */
@Configuration
public class FilterConfig {

    @Bean
    public FilterRegistrationBean unregisterTokenAuthenticationFilter(TokenAuthenticationFilter filter) {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setFilter(filter);
        filterRegistrationBean.setEnabled(false);
        return filterRegistrationBean;
    }

}
