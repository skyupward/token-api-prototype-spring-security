package top.ourway.prototype.springsecurity.rest.security;

import org.springframework.security.core.userdetails.UserDetails;

/**
 * Created by skyupward on 2015/5/23.
 */

public interface TokenManager {

    String createToken(UserDetails userDetails);

    UserDetails removeToken(String token);

    UserDetails getUserDetails(String token);

}
