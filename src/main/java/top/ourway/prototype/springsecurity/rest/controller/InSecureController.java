package top.ourway.prototype.springsecurity.rest.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by skyupward on 2015/5/23.
 */
@RestController
public class InSecureController {

    @RequestMapping("/hello")
    public String sayHelloToEveryOne() {
        return "Hello! You can see me even you have not been authenticated!";
    }

}
